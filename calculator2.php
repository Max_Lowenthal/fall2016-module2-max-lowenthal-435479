<!DOCTYPE html>
<html>
	<head>
		<title> Calculator </title>
		<link rel="stylesheet" type="text/css" href="calcStyle.css" />
	</head>
	<body>
			<div class="bigger">
				<p>Calculator!</p>
			</div>
			
			<form method = "POST">
			Input 1: <input type="number" name = "n1">
			<div class="lowerdrop">
			Input 2: <input type="number" name = "n2">
			</div>
			
			<div class="drop">
			Add: <input type="radio" name="Operation" value="add">
			Subtract: <input type="radio" name="Operation" value ="subtract">
			Multiply: <input type="radio" name="Operation" value ="multiply">
			Divide: <input type="radio" name="Operation" value ="divide">
			</div>
			
			<input type="Submit" value="Calculate">
			
			</form>
		
		<?php
			if(isset($_POST['n1']) && isset($_POST['n2']) && isset($_POST['Operation'])){
				$n1 = $_POST['n1'];
				$n2 = $_POST['n2'];
				$op = $_POST['Operation'];
			
					if($op == 'subtract'){
						echo $n1-$n2;
					}
					else if($op =='add'){
						echo $n1+$n2;
					}
					else if($op =='multiply'){
						echo $n1*$n2;
					}
					else if($op == 'divide' && $n2 == 0){
						echo "You cannot divide by 0!";
					}
					else if ($op == 'divide'){
						echo $n1/$n2;
					}
					
					else{
						echo "You can't do that!";
					}
			
			}
			?>
	</body>
</html>